---
title: Get Involved!
---

Fedora Kinoite is maintained with love by volunters. By joining the Fedora and KDE team,
you will be part of an international community working to deliver a stunning Free Software
computing experience.

This page will give you a brief introduction to things everyone in KDE should know, and
help you get started with contributing.

Kinoite is based on the same technology as Fedora CoreOS and Fedora Silverblue. It is
maintained by the [Fedora KDE Special Interest Group (SIG)](https://fedoraproject.org/wiki/SIGs/KDE)
and is the fruit of collaboration between the Fedora and KDE community. 

The first step in contributing to Kinoite is to join us on our [Matrix channel](matrix.to/#/#fedora-kde:matrix.org)
or IRC room [#fedora-kde](irc://irc.libera.chat/fedora-kde). Feel free to ask us any questions you may have.

## Get Involved in Fedora

The Fedora community is working on the way to package all the individual components to
deliver an optimal experience to the user. You can find more information on how to get
involved on the generic [Fedora get involved page](https://fedoraproject.org/wiki/Join).

## Get Involved in KDE

The KDE community creates the software and they also have a [get involved page](https://community.kde.org/Get_Involved)
with a lot of helpful information on how to help.
